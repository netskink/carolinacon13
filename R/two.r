# this is for quiz one.
# 
# Consider the setting where our inputs are 1-dimensional. We have data
# 
# x	y
# 2.5	+1
# 0.3	-1
# 2.8	+1
# 0.5	+1
#
# and the current estimates of the weights are w0=0 and w1=1. 
#
# 	w0: the intercept, 
#  	w1: the weight for x
# 
# Calculate the likelihood of this data. Round your answer to 2 decimal places.

# Likelihood is a function identified with script l.
# It is a quality metric which we use with gradient descent to find the best fit of a line.
# It is a function of the weights.  The function which seperates the postive sentiment
# samples from the negative sentiment values has an associated likelihood.  He shows and
# example graph with the lines of various slopes and intercepts which seperate the 
# positive from the negative reviews.  He shows the liklihood for each line to vary from
# 10^-6 to 10^-4 where the 10^-4 is the best.  He says they are all less than one but the
# closet they are to 1 the better.  
x=c(2.5, 0.3, 2.8, 0.5)
y=c(1,-1,+1,+1)
w=c(0, 1)
X=matrix(x,4,1)
Y=matrix(y,1,4)
W=matrix(w,1,2)

# some math tests for example
# source (file.path("one.R"))

a=c(0,1,2)
b=c(2,1)

A=matrix(a,3,1)
B=matrix(b,1,2)

#print(A)
#print(B)

# matrix multiply
#res=A%*%B
# elementwise multiply - will fail
# res=A*B
#print(res)

link <- function(x) {
    y=1/(1+exp(-x))
    return (y)
}

prob <- function(x,y) {
    if (y<0) {
        return (1-x)
    } else {
        return (x)
    }
}

#score=
#res = link()

#print(res)


print(X)
#print(Y)
print(W)


res=X%*%W
print(res)
res = link(res)
print(res)
# I want to do prob() for each element of x,y
# apply() only does one parm for the function
# mapply can have multiple args
res = matrix(mapply(prob,res,y),c(4,2))
print(res)

liklihood=prod(res[,2])
print(liklihood)

loglik=log(res)
print(loglik)
print("one way")
print(sum(loglik[,2]))
print("another way")
loglik=sum(log(res)[,2])
print(loglik)
print("yet another way")
print(log(liklihood))

# the log likelihood part was not right.  Perhaps the derivative part in question is key.


# Calculate the derivative of the log likelihood with respect to w1. 
# Round your answer to 2 decimal places.

# derivative is sum[i=1..N] of  h1(xi) [ (indicator(yi=+1) - prob(yi=+1|xi,w1) ]

# We only have one x and w1=1 so h1(x) = 1*x
#
#  This one uses P(y=+1) to be 1-P() for the -1 case
#   x       y       P(y=+1|xi,w1)   Contribution to derivative for w1
# ------------------------------------------------------------------
#   2.5     +1      0.9241          1*2.5[ 1 - 0.9241] = 0.18975
#   0.3     -1      0.4256          1*0.3[ 0 - 0.4256] = -0.12768
#   2.8     +1      0.9427          1*2.8[ 1 - 0.9427] = 0.16044
#   0.5     +1      0.6225          1*0.5[ 1 - 0.6225] = 0.18875
#                                                  sum = 0.41126  wrong ans
#                                              log sum = -0.89  wrong ans


#  This one uses P(y=+1) to be P() for the -1 case
#   x       y       P(y=+1|xi,w1)   Contribution to derivative for w1
# ------------------------------------------------------------------
#   2.5     +1      0.9241          1*2.5[ 1 - 0.9241] = 0.18975
#   0.3     -1      0.5744          1*0.3[ 0 - 0.5744] = -0.17232
#   2.8     +1      0.9427          1*2.8[ 1 - 0.9427] = 0.16044
#   0.5     +1      0.6225          1*0.5[ 1 - 0.6225] = 0.18875
#                                                  sum = 0.36662 correct
#                                              log sum = -1.003429  wrong ans



# b=1*0.3*( 0 - 0.4256)
# c=1*2.8*( 0 - 0.9427)
# d=1*0.5*( 1 - 0.6225)
# c=1*2.8*( 1 - 0.9427)
# a=1*2.5*( 1 - 0.9241)
# b=1*0.3*( 0 - 0.4256)
# c=1*2.8*( 1 - 0.9427)
# d=1*0.5*( 1 - 0.6225)
# a+b+c+d
# log(a+b+c+d)
# b=1*0.3*( 0 - 0.5744)
# b
# a+b+c+d
# log(a+b+c+d)




