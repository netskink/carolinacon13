library(MASS)



# in R session
# source (file.path("q1.r"))



# the input samples
x=c(-0.14,  0.24,  0.71,  0.87,  0.83,
     0.78,  0.89,  0.82,  0.66,  0.29, 
    -0.14, -0.31, -0.74, -0.93, -1.03, 
    -1.12, -0.96, -0.87, -0.57, -0.30, 
     0.02,  0.32,  0.77,  0.91,  0.98, 
     0.85,  0.76,  0.56,  0.77,  0.32, 
    -0.07, -0.50, -0.78, -0.81, -0.82, 
    -1.16, -0.95, -0.74, -0.69, -0.11, 
     0.09,  0.62,  0.65,  0.92,  0.87, 
     0.93,  0.78,  0.75,  0.59,  0.10,
    -0.11, -0.51, -0.70, -0.78, -0.98,
    -1.09, -0.97, -0.71, -0.45, -0.21, 
     0.21,  0.61,  0.71,  0.89,  0.77, 
     0.90,  0.73,  0.63,  0.45,  0.25, 
    -0.04, -0.53, -0.79, -0.84, -0.95, 
    -0.90, -0.77, -0.79, -0.32, -0.07, 
     0.18,  0.47,  0.90,  0.85,  0.88, 
     0.99,  1.03,  0.62,  0.37, -0.06, 
    -0.11, -0.55, -0.74, -1.04, -0.91,
    -0.92, -0.93, -0.59, -0.36,  0.01)

last_five=c(-1.04, -0.91, -0.92, -0.93, -0.59)

# linear model is of this form:
# [five inputs] * a = predicted sixth value
# 
# a is our linear prediction coefficients
# 
# So given five input values, then the sixth value is predicted to be:
#
# -0.14*a4 + 0.24*a3 + 0.71*a2 + 0.87*a1 + 0.83*a0 = 0.78
#
# Likewise, slide the input by one and the seventh value is predicted to be:
#
# 0.24*a4 + 0.71*a3 + 0.87*a2 + 0.83*a1 + 0.78*a0 = 0.89
#
# This leads to for the first 5 predicted values:
#
# -0.14, 0.24, 0.71, 0.87, 0.83 -> 0.78
#  0.24, 0.71, 0.87, 0.83, 0.78 -> 0.89
#  0.71, 0.87, 0.83, 0.78, 0.89 -> 0.82
#  0.87, 0.83, 0.78, 0.89, 0.82 -> 0.66
#  0.83, 0.78, 0.89, 0.82, 0.66 -> 0.29

# So the overall linear model coeficients uses all input data
# to determine the coefficients for the predictor.
# 
# [ first five inputs        [ a4        [ the sixth input
#   second five inputs         a3          the seventh input
#   third five inputs     *    a2    =     the eighth input
#   ....                       a1          ...
#   last five inputs  ]        a0 ]        the last input ]
#
# Refer to the input matrix as X
#
#
#  This equation is solved so that we get the linear prediction coefficients "a"
#
#  Then we can use this equation to predict the sixth value given five values
#  like so:
#
#  (realizing we have to transpose one of the arrays to obey matrix rules of multiplication)
#
# [ first input       [ a4        
#   second input        a3        
#   third input     *   a2    =  predicted sixth input
#   fourth              a1       
#   fifth input  ]      a0 ]     


# just testing this out
fudge0=c(-0.14, 0.24, 0.71, 0.87, 0.83)
fudge1=c( 0.24, 0.71, 0.87, 0.83, 0.78)
fudge2=c( 0.71, 0.87, 0.83, 0.78, 0.89)
fudge3=c( 0.87, 0.83, 0.78, 0.89, 0.82)
fudge4=c( 0.83, 0.78, 0.89, 0.82, 0.66)

# lets create a matrix of the first five rows
X=matrix(c(fudge0,fudge1,fudge2,fudge3,fudge4),5,5)

# lets create a results array of the first 5 predicted values
p=c(0.78, 0.89, 0.82, 0.66, 0.29)


# solving for a gives
#
# a = inverse(XTranspose * X)  * XTranspose  * p
#
#  the first part is called the pseudo_inverse


first_part = ginv(t(X) %*% X)
second_part = t(X)

pseudo_inv = first_part %*% second_part

a=pseudo_inv %*% p

# Check if correct.  
# Given the five inputs and the linear prediction coefficients a does it 
# give a predicted value which matches our input?
#
# Multiply the five value input vector by the five value coefficients:
# For each of the 5 inputs, it should match the output
# ie. [inputs vec] * [a] = sixth value

print(paste0("sixth value is ", fudge0 %*% a, ". Should be 0.78"))
print(paste0("seventh value is ", fudge1 %*% a, ". Should be 0.89"))
print(paste0("eighth value is ", fudge2 %*% a, ". Should be 0.82"))
print(paste0("ninth value is ", fudge3 %*% a, ". Should be 0.66"))
print(paste0("tenth value is ", fudge4 %*% a, ". Should be 0.29"))




#
# That works.
#
# Now, we need to determine the coefficients vector using all the 
# input data stream instead of just the first ten values.
#
# Bascially given x input vector, make X output vector
# and p vector.

makeXFsckUP <- function(x) {

    # take first five values as a row
    # take next five values append as a row. repeat until done
    result = matrix(NA,length(x)/5,5)  # create a matrix of size(nrows, 5) of NA values. The nrows is equal to length of input / 5

    input_counter = 0
    row_counter = 1

    for (val in x) {
        # if its a new row, start a new row vector
        if (input_counter %% 5 == 0) {
            a_new_row = c(val)
        } else {
            # append values to new row vector
            a_new_row = c(a_new_row, val)
        }

        print(paste0(input_counter, " ", a_new_row))

        # row and columns count from 1 in a matrix
        # foo[0,] is a fsck up

        # if its a full row, assign the row to our output matrix
        if (input_counter %% 5 == 4) {  # if we have five values, its a full row
            print(a_new_row)
            result[row_counter,] = a_new_row  # indexes start counting at 1
            row_counter = row_counter + 1
        }


        input_counter = input_counter + 1


    }
    return(result)
}



makeX <- function(x) {

    # take first five values as a row
    # take next five values append as a row. repeat until done
    result = matrix(NA,length(x)-5,5)  # create a matrix of size(nrows, 5) of NA values. The nrows is equal to length of input / 5

    input_counter = 0
    row_counter = 1

    i = 1
    while (i < length(x)-4 ) {

    	print(paste0("i = ", i))
      result[row_counter,1] = x[i+0]
      result[row_counter,2] = x[i+1]
      result[row_counter,3] = x[i+2]
      result[row_counter,4] = x[i+3]
      result[row_counter,5] = x[i+4]
      i = i + 1
      row_counter = row_counter + 1
    }

    return(result)
}



result = makeX(x)

# use our function to make a larger X matrix
X=result
# now, we can use all our available values for p, rather than
# the first five values
p=x[6:100]

# determine the linear predictor coefficients again
first_part = ginv(t(X) %*% X)
second_part = t(X)

pseudo_inv = first_part %*% second_part

a=pseudo_inv %*% p


# last_five=c(-1.04, -0.91, -0.92, -0.93, -0.59)
# he says it should be
# -0.3016
# last_five %*% p

print(paste0("given last value vector ", last_five %*% a, ". The data says -0.36, but he says it should be -0.3016"))

# does our first values still work
print(paste0("sixth value is ", fudge0 %*% a, ". Should be 0.78"))
print(paste0("seventh value is ", fudge1 %*% a, ". Should be 0.89"))
print(paste0("eighth value is ", fudge2 %*% a, ". Should be 0.82"))
print(paste0("ninth value is ", fudge3 %*% a, ". Should be 0.66"))
print(paste0("tenth value is ", fudge4 %*% a, ". Should be 0.29"))
print(paste0("last value is ", X[95,] %*% a, ". Should be 0.01"))

plot(x,type="l")
