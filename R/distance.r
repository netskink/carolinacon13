
# in R session
# source (file.path("q1.r"))

#  Consider the following two sentences.
#  
#  Sentence 1: The quick brown fox jumps over the lazy dog.
#  Sentence 2: A quick brown dog outpaces a quick fox.

#  Compute the Euclidean distance using word counts. To compute word counts,
#  turn all words into lower case and strip all punctuation, so that "The" and
#  "the" are counted as the same token. That is, document 1 would be represented
#  as 
#  
#  x=[# the,# a,# quick,# brown,# fox,# jumps,# over,# lazy,# dog,# outpaces]
#  where # word is the count of that word in the document.
#  
#  Round your answer to 3 decimal places.

#      t a q b f j o l d o
#      h   u r o u v a o u
#      e   i o x m e z g t
s1 = c(2,0,1,1,1,1,1,1,1,0)
s2 = c(0,2,2,1,1,0,0,0,1,1)

S1=matrix(s1,1,10)
#print(S1)
S2=matrix(s2,1,10)
#print(S2)

# euclidian distance
distance=S1-S2
distance=distance %*% t(distance)
distance=sqrt(distance)
print(paste0("eculidian distance = ",distance))


# cosine distance
# cosine distance = 1 - cos similarity = 1 - xTy / ||x|| ||y||

# magnitude of x is the sqrt of sum of sq's of each element
# elementwise mult is simply *
magX = S1*S1
magX = sum(magX)
magX = sqrt(magX)
print(magX)
magY = S2*S2
magY = sum(magY)
magY = sqrt(magY)
print(magY)

similarity = S1 %*% t(S2)
similarity = similarity / (magX * magY)
print(paste0("cosine similarity = ",similarity))

cos_distance = 1 - similarity
print(paste0("cosine distance = ",cos_distance))







