function [ CombinedSamples ] = mymake_csv( Features )
%make csv file Summary of this function goes here
%   Detailed explanation goes here

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% soldier samples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% mean %%%%%%%%%%%%%%%%%%
mean_zcr=Features{1,1}(1,:)';
mean_energy=Features{1,1}(2,:)';

mean_energy_entropy=Features{1,1}(3,:)';
mean_spectral_centroid_var=Features{1,1}(4,:)';

mean_spectral_centroid_spread=Features{1,1}(5,:)';
mean_spectral_entropy=Features{1,1}(6,:)';

mean_spectral_flux=Features{1,1}(7,:)';
mean_spectral_rolloff=Features{1,1}(8,:)';

mean_mfccs1=Features{1,1}(9,:)';
mean_mfccs2=Features{1,1}(10,:)';

mean_mfccs3=Features{1,1}(11,:)';
mean_mfccs4=Features{1,1}(12,:)';
mean_mfccs5=Features{1,1}(13,:)';
mean_mfccs6=Features{1,1}(14,:)';
mean_mfccs7=Features{1,1}(15,:)';
mean_mfccs8=Features{1,1}(16,:)';
mean_mfccs9=Features{1,1}(17,:)';
mean_mfccs10=Features{1,1}(18,:)';
mean_mfccs11=Features{1,1}(19,:)';
mean_mfccs12=Features{1,1}(20,:)';
mean_mfccs13=Features{1,1}(21,:)';
mean_harmonic_hr=Features{1,1}(22,:)';

mean_harmonic_f0=Features{1,1}(23,:)';
mean_chroma_vector1=Features{1,1}(24,:)';

mean_chroma_vector2=Features{1,1}(25,:)';
mean_chroma_vector3=Features{1,1}(26,:)';
mean_chroma_vector4=Features{1,1}(27,:)';
mean_chroma_vector5=Features{1,1}(28,:)';
mean_chroma_vector6=Features{1,1}(29,:)';
mean_chroma_vector7=Features{1,1}(30,:)';
mean_chroma_vector8=Features{1,1}(31,:)';
mean_chroma_vector9=Features{1,1}(32,:)';
mean_chroma_vector10=Features{1,1}(33,:)';
mean_chroma_vector11=Features{1,1}(34,:)';
mean_chroma_vector12=Features{1,1}(35,:)';


%%%%%%%%%%% median %%%%%%%%%%%%%%%%%%
offset=35;
median_zcr=Features{1,1}(1+offset,:)';
median_energy=Features{1,1}(2+offset,:)';

median_energy_entropy=Features{1,1}(3+offset,:)';
median_spectral_centroid_var=Features{1,1}(4+offset,:)';

median_spectral_centroid_spread=Features{1,1}(5+offset,:)';
median_spectral_entropy=Features{1,1}(6+offset,:)';

median_spectral_flux=Features{1,1}(7+offset,:)';
median_spectral_rolloff=Features{1,1}(8+offset,:)';

median_mfccs1=Features{1,1}(9+offset,:)';
median_mfccs2=Features{1,1}(10+offset,:)';

median_mfccs3=Features{1,1}(11+offset,:)';
median_mfccs4=Features{1,1}(12+offset,:)';
median_mfccs5=Features{1,1}(13+offset,:)';
median_mfccs6=Features{1,1}(14+offset,:)';
median_mfccs7=Features{1,1}(15+offset,:)';
median_mfccs8=Features{1,1}(16+offset,:)';
median_mfccs9=Features{1,1}(17+offset,:)';
median_mfccs10=Features{1,1}(18+offset,:)';
median_mfccs11=Features{1,1}(19+offset,:)';
median_mfccs12=Features{1,1}(20+offset,:)';
median_mfccs13=Features{1,1}(21+offset,:)';
median_harmonic_hr=Features{1,1}(22+offset,:)';

median_harmonic_f0=Features{1,1}(23+offset,:)';
median_chroma_vector1=Features{1,1}(24+offset,:)';

median_chroma_vector2=Features{1,1}(25+offset,:)';
median_chroma_vector3=Features{1,1}(26+offset,:)';
median_chroma_vector4=Features{1,1}(27+offset,:)';
median_chroma_vector5=Features{1,1}(28+offset,:)';
median_chroma_vector6=Features{1,1}(29+offset,:)';
median_chroma_vector7=Features{1,1}(30+offset,:)';
median_chroma_vector8=Features{1,1}(31+offset,:)';
median_chroma_vector9=Features{1,1}(32+offset,:)';
median_chroma_vector10=Features{1,1}(33+offset,:)';
median_chroma_vector11=Features{1,1}(34+offset,:)';
median_chroma_vector12=Features{1,1}(35+offset,:)';



%%%%%%%%%%% std %%%%%%%%%%%%%%%%%%
offset=70;
std_zcr=Features{1,1}(1+offset,:)';
std_energy=Features{1,1}(2+offset,:)';

std_energy_entropy=Features{1,1}(3+offset,:)';
std_spectral_centroid_var=Features{1,1}(4+offset,:)';

std_spectral_centroid_spread=Features{1,1}(5+offset,:)';
std_spectral_entropy=Features{1,1}(6+offset,:)';

std_spectral_flux=Features{1,1}(7+offset,:)';
std_spectral_rolloff=Features{1,1}(8+offset,:)';

std_mfccs1=Features{1,1}(9+offset,:)';
std_mfccs2=Features{1,1}(10+offset,:)';

std_mfccs3=Features{1,1}(11+offset,:)';
std_mfccs4=Features{1,1}(12+offset,:)';
std_mfccs5=Features{1,1}(13+offset,:)';
std_mfccs6=Features{1,1}(14+offset,:)';
std_mfccs7=Features{1,1}(15+offset,:)';
std_mfccs8=Features{1,1}(16+offset,:)';
std_mfccs9=Features{1,1}(17+offset,:)';
std_mfccs10=Features{1,1}(18+offset,:)';
std_mfccs11=Features{1,1}(19+offset,:)';
std_mfccs12=Features{1,1}(20+offset,:)';
std_mfccs13=Features{1,1}(21+offset,:)';
std_harmonic_hr=Features{1,1}(22+offset,:)';

std_harmonic_f0=Features{1,1}(23+offset,:)';
std_chroma_vector1=Features{1,1}(24+offset,:)';

std_chroma_vector2=Features{1,1}(25+offset,:)';
std_chroma_vector3=Features{1,1}(26+offset,:)';
std_chroma_vector4=Features{1,1}(27+offset,:)';
std_chroma_vector5=Features{1,1}(28+offset,:)';
std_chroma_vector6=Features{1,1}(29+offset,:)';
std_chroma_vector7=Features{1,1}(30+offset,:)';
std_chroma_vector8=Features{1,1}(31+offset,:)';
std_chroma_vector9=Features{1,1}(32+offset,:)';
std_chroma_vector10=Features{1,1}(33+offset,:)';
std_chroma_vector11=Features{1,1}(34+offset,:)';
std_chroma_vector12=Features{1,1}(35+offset,:)';


%%%%%%%%%%% stdbymean %%%%%%%%%%%%%%%%%%
offset=70+35;
stdbymean_zcr=Features{1,1}(1+offset,:)';
stdbymean_energy=Features{1,1}(2+offset,:)';

stdbymean_energy_entropy=Features{1,1}(3+offset,:)';
stdbymean_spectral_centroid_var=Features{1,1}(4+offset,:)';

stdbymean_spectral_centroid_spread=Features{1,1}(5+offset,:)';
stdbymean_spectral_entropy=Features{1,1}(6+offset,:)';

stdbymean_spectral_flux=Features{1,1}(7+offset,:)';
stdbymean_spectral_rolloff=Features{1,1}(8+offset,:)';

stdbymean_mfccs1=Features{1,1}(9+offset,:)';
stdbymean_mfccs2=Features{1,1}(10+offset,:)';

stdbymean_mfccs3=Features{1,1}(11+offset,:)';
stdbymean_mfccs4=Features{1,1}(12+offset,:)';
stdbymean_mfccs5=Features{1,1}(13+offset,:)';
stdbymean_mfccs6=Features{1,1}(14+offset,:)';
stdbymean_mfccs7=Features{1,1}(15+offset,:)';
stdbymean_mfccs8=Features{1,1}(16+offset,:)';
stdbymean_mfccs9=Features{1,1}(17+offset,:)';
stdbymean_mfccs10=Features{1,1}(18+offset,:)';
stdbymean_mfccs11=Features{1,1}(19+offset,:)';
stdbymean_mfccs12=Features{1,1}(20+offset,:)';
stdbymean_mfccs13=Features{1,1}(21+offset,:)';
stdbymean_harmonic_hr=Features{1,1}(22+offset,:)';

stdbymean_harmonic_f0=Features{1,1}(23+offset,:)';
stdbymean_chroma_vector1=Features{1,1}(24+offset,:)';

stdbymean_chroma_vector2=Features{1,1}(25+offset,:)';
stdbymean_chroma_vector3=Features{1,1}(26+offset,:)';
stdbymean_chroma_vector4=Features{1,1}(27+offset,:)';
stdbymean_chroma_vector5=Features{1,1}(28+offset,:)';
stdbymean_chroma_vector6=Features{1,1}(29+offset,:)';
stdbymean_chroma_vector7=Features{1,1}(30+offset,:)';
stdbymean_chroma_vector8=Features{1,1}(31+offset,:)';
stdbymean_chroma_vector9=Features{1,1}(32+offset,:)';
stdbymean_chroma_vector10=Features{1,1}(33+offset,:)';
stdbymean_chroma_vector11=Features{1,1}(34+offset,:)';
stdbymean_chroma_vector12=Features{1,1}(35+offset,:)';



%%%%%%%%%%% max %%%%%%%%%%%%%%%%%%
offset=70+70;
max_zcr=Features{1,1}(1+offset,:)';
max_energy=Features{1,1}(2+offset,:)';

max_energy_entropy=Features{1,1}(3+offset,:)';
max_spectral_centroid_var=Features{1,1}(4+offset,:)';

max_spectral_centroid_spread=Features{1,1}(5+offset,:)';
max_spectral_entropy=Features{1,1}(6+offset,:)';

max_spectral_flux=Features{1,1}(7+offset,:)';
max_spectral_rolloff=Features{1,1}(8+offset,:)';

max_mfccs1=Features{1,1}(9+offset,:)';
max_mfccs2=Features{1,1}(10+offset,:)';

max_mfccs3=Features{1,1}(11+offset,:)';
max_mfccs4=Features{1,1}(12+offset,:)';
max_mfccs5=Features{1,1}(13+offset,:)';
max_mfccs6=Features{1,1}(14+offset,:)';
max_mfccs7=Features{1,1}(15+offset,:)';
max_mfccs8=Features{1,1}(16+offset,:)';
max_mfccs9=Features{1,1}(17+offset,:)';
max_mfccs10=Features{1,1}(18+offset,:)';
max_mfccs11=Features{1,1}(19+offset,:)';
max_mfccs12=Features{1,1}(20+offset,:)';
max_mfccs13=Features{1,1}(21+offset,:)';
max_harmonic_hr=Features{1,1}(22+offset,:)';

max_harmonic_f0=Features{1,1}(23+offset,:)';
max_chroma_vector1=Features{1,1}(24+offset,:)';

max_chroma_vector2=Features{1,1}(25+offset,:)';
max_chroma_vector3=Features{1,1}(26+offset,:)';
max_chroma_vector4=Features{1,1}(27+offset,:)';
max_chroma_vector5=Features{1,1}(28+offset,:)';
max_chroma_vector6=Features{1,1}(29+offset,:)';
max_chroma_vector7=Features{1,1}(30+offset,:)';
max_chroma_vector8=Features{1,1}(31+offset,:)';
max_chroma_vector9=Features{1,1}(32+offset,:)';
max_chroma_vector10=Features{1,1}(33+offset,:)';
max_chroma_vector11=Features{1,1}(34+offset,:)';
max_chroma_vector12=Features{1,1}(35+offset,:)';




%%%%%%%%%%% min %%%%%%%%%%%%%%%%%%
offset=70+70+35;
min_zcr=Features{1,1}(1+offset,:)';
min_energy=Features{1,1}(2+offset,:)';

min_energy_entropy=Features{1,1}(3+offset,:)';
min_spectral_centroid_var=Features{1,1}(4+offset,:)';

min_spectral_centroid_spread=Features{1,1}(5+offset,:)';
min_spectral_entropy=Features{1,1}(6+offset,:)';

min_spectral_flux=Features{1,1}(7+offset,:)';
min_spectral_rolloff=Features{1,1}(8+offset,:)';

min_mfccs1=Features{1,1}(9+offset,:)';
min_mfccs2=Features{1,1}(10+offset,:)';

min_mfccs3=Features{1,1}(11+offset,:)';
min_mfccs4=Features{1,1}(12+offset,:)';
min_mfccs5=Features{1,1}(13+offset,:)';
min_mfccs6=Features{1,1}(14+offset,:)';
min_mfccs7=Features{1,1}(15+offset,:)';
min_mfccs8=Features{1,1}(16+offset,:)';
min_mfccs9=Features{1,1}(17+offset,:)';
min_mfccs10=Features{1,1}(18+offset,:)';
min_mfccs11=Features{1,1}(19+offset,:)';
min_mfccs12=Features{1,1}(20+offset,:)';
min_mfccs13=Features{1,1}(21+offset,:)';
min_harmonic_hr=Features{1,1}(22+offset,:)';

min_harmonic_f0=Features{1,1}(23+offset,:)';
min_chroma_vector1=Features{1,1}(24+offset,:)';

min_chroma_vector2=Features{1,1}(25+offset,:)';
min_chroma_vector3=Features{1,1}(26+offset,:)';
min_chroma_vector4=Features{1,1}(27+offset,:)';
min_chroma_vector5=Features{1,1}(28+offset,:)';
min_chroma_vector6=Features{1,1}(29+offset,:)';
min_chroma_vector7=Features{1,1}(30+offset,:)';
min_chroma_vector8=Features{1,1}(31+offset,:)';
min_chroma_vector9=Features{1,1}(32+offset,:)';
min_chroma_vector10=Features{1,1}(33+offset,:)';
min_chroma_vector11=Features{1,1}(34+offset,:)';
min_chroma_vector12=Features{1,1}(35+offset,:)';

SoldierTable=table(mean_zcr, mean_energy, ...
        mean_energy_entropy, mean_spectral_centroid_var, ...
        mean_spectral_centroid_spread, mean_spectral_entropy, ...
        mean_spectral_flux, mean_spectral_rolloff, ...
        mean_mfccs1, mean_mfccs2, ...
        mean_mfccs3, mean_mfccs4, ...
        mean_mfccs5, mean_mfccs6, ...
        mean_mfccs7, mean_mfccs8, ...
        mean_mfccs9, mean_mfccs10, ...
        mean_mfccs11, mean_mfccs12, ...
        mean_mfccs13, mean_harmonic_hr, ...
        mean_harmonic_f0, mean_chroma_vector1, ...
        mean_chroma_vector2, mean_chroma_vector3, ...
        mean_chroma_vector4, mean_chroma_vector5, ...
        mean_chroma_vector6, mean_chroma_vector7, ...
        mean_chroma_vector8, mean_chroma_vector9, ...
        mean_chroma_vector10, mean_chroma_vector11, ...
        mean_chroma_vector12, ...  
        median_zcr, median_energy, ...
        median_energy_entropy, median_spectral_centroid_var, ...
        median_spectral_centroid_spread, median_spectral_entropy, ...
        median_spectral_flux, median_spectral_rolloff, ...
        median_mfccs1, median_mfccs2, ...
        median_mfccs3, median_mfccs4, ...
        median_mfccs5, median_mfccs6, ...
        median_mfccs7, median_mfccs8, ...
        median_mfccs9, median_mfccs10, ...
        median_mfccs11, median_mfccs12, ...
        median_mfccs13, median_harmonic_hr, ...
        median_harmonic_f0, median_chroma_vector1, ...
        median_chroma_vector2, median_chroma_vector3, ...
        median_chroma_vector4, median_chroma_vector5, ...
        median_chroma_vector6, median_chroma_vector7, ...
        median_chroma_vector8, median_chroma_vector9, ...
        median_chroma_vector10, median_chroma_vector11, ...
        median_chroma_vector12, ...  
        std_zcr, std_energy, ...
        std_energy_entropy, std_spectral_centroid_var, ...
        std_spectral_centroid_spread, std_spectral_entropy, ...
        std_spectral_flux, std_spectral_rolloff, ...
        std_mfccs1, std_mfccs2, ...
        std_mfccs3, std_mfccs4, ...
        std_mfccs5, std_mfccs6, ...
        std_mfccs7, std_mfccs8, ...
        std_mfccs9, std_mfccs10, ...
        std_mfccs11, std_mfccs12, ...
        std_mfccs13, std_harmonic_hr, ...
        std_harmonic_f0, std_chroma_vector1, ...
        std_chroma_vector2, std_chroma_vector3, ...
        std_chroma_vector4, std_chroma_vector5, ...
        std_chroma_vector6, std_chroma_vector7, ...
        std_chroma_vector8, std_chroma_vector9, ...
        std_chroma_vector10, std_chroma_vector11, ...
        std_chroma_vector12, ...
        stdbymean_zcr, stdbymean_energy, ...
        stdbymean_energy_entropy, stdbymean_spectral_centroid_var, ...
        stdbymean_spectral_centroid_spread, stdbymean_spectral_entropy, ...
        stdbymean_spectral_flux, stdbymean_spectral_rolloff, ...
        stdbymean_mfccs1, stdbymean_mfccs2, ...
        stdbymean_mfccs3, stdbymean_mfccs4, ...
        stdbymean_mfccs5, stdbymean_mfccs6, ...
        stdbymean_mfccs7, stdbymean_mfccs8, ...
        stdbymean_mfccs9, stdbymean_mfccs10, ...
        stdbymean_mfccs11, stdbymean_mfccs12, ...
        stdbymean_mfccs13, stdbymean_harmonic_hr, ...
        stdbymean_harmonic_f0, stdbymean_chroma_vector1, ...
        stdbymean_chroma_vector2, stdbymean_chroma_vector3, ...
        stdbymean_chroma_vector4, stdbymean_chroma_vector5, ...
        stdbymean_chroma_vector6, stdbymean_chroma_vector7, ...
        stdbymean_chroma_vector8, stdbymean_chroma_vector9, ...
        stdbymean_chroma_vector10, stdbymean_chroma_vector11, ...
        stdbymean_chroma_vector12, ...
        max_zcr, max_energy, ...
        max_energy_entropy, max_spectral_centroid_var, ...
        max_spectral_centroid_spread, max_spectral_entropy, ...
        max_spectral_flux, max_spectral_rolloff, ...
        max_mfccs1, max_mfccs2, ...
        max_mfccs3, max_mfccs4, ...
        max_mfccs5, max_mfccs6, ...
        max_mfccs7, max_mfccs8, ...
        max_mfccs9, max_mfccs10, ...
        max_mfccs11, max_mfccs12, ...
        max_mfccs13, max_harmonic_hr, ...
        max_harmonic_f0, max_chroma_vector1, ...
        max_chroma_vector2, max_chroma_vector3, ...
        max_chroma_vector4, max_chroma_vector5, ...
        max_chroma_vector6, max_chroma_vector7, ...
        max_chroma_vector8, max_chroma_vector9, ...
        max_chroma_vector10, max_chroma_vector11, ...
        max_chroma_vector12, ...
        min_zcr, min_energy, ...
        min_energy_entropy, min_spectral_centroid_var, ...
        min_spectral_centroid_spread, min_spectral_entropy, ...
        min_spectral_flux, min_spectral_rolloff, ...
        min_mfccs1, min_mfccs2, ...
        min_mfccs3, min_mfccs4, ...
        min_mfccs5, min_mfccs6, ...
        min_mfccs7, min_mfccs8, ...
        min_mfccs9, min_mfccs10, ...
        min_mfccs11, min_mfccs12, ...
        min_mfccs13, min_harmonic_hr, ...
        min_harmonic_f0, min_chroma_vector1, ...
        min_chroma_vector2, min_chroma_vector3, ...
        min_chroma_vector4, min_chroma_vector5, ...
        min_chroma_vector6, min_chroma_vector7, ...
        min_chroma_vector8, min_chroma_vector9, ...
        min_chroma_vector10, min_chroma_vector11, ...
        min_chroma_vector12 ...
        );

SoldierTable.label=repmat('soldier ',14,1);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% gameplay samples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% mean %%%%%%%%%%%%%%%%%%
mean_zcr=Features{1,2}(1,:)';
mean_energy=Features{1,2}(2,:)';
mean_energy_entropy=Features{1,2}(3,:)';
mean_spectral_centroid_var=Features{1,2}(4,:)';
mean_spectral_centroid_spread=Features{1,2}(5,:)';
mean_spectral_entropy=Features{1,2}(6,:)';
mean_spectral_flux=Features{1,2}(7,:)';
mean_spectral_rolloff=Features{1,2}(8,:)';
mean_mfccs1=Features{1,2}(9,:)';
mean_mfccs2=Features{1,2}(10,:)';
mean_mfccs3=Features{1,2}(11,:)';
mean_mfccs4=Features{1,2}(12,:)';
mean_mfccs5=Features{1,2}(13,:)';
mean_mfccs6=Features{1,2}(14,:)';
mean_mfccs7=Features{1,2}(15,:)';
mean_mfccs8=Features{1,2}(16,:)';
mean_mfccs9=Features{1,2}(17,:)';
mean_mfccs10=Features{1,2}(18,:)';
mean_mfccs11=Features{1,2}(19,:)';
mean_mfccs12=Features{1,2}(20,:)';
mean_mfccs13=Features{1,2}(21,:)';
mean_harmonic_hr=Features{1,2}(22,:)';
mean_harmonic_f0=Features{1,2}(23,:)';
mean_chroma_vector1=Features{1,2}(24,:)';
mean_chroma_vector2=Features{1,2}(25,:)';
mean_chroma_vector3=Features{1,2}(26,:)';
mean_chroma_vector4=Features{1,2}(27,:)';
mean_chroma_vector5=Features{1,2}(28,:)';
mean_chroma_vector6=Features{1,2}(29,:)';
mean_chroma_vector7=Features{1,2}(30,:)';
mean_chroma_vector8=Features{1,2}(31,:)';
mean_chroma_vector9=Features{1,2}(32,:)';
mean_chroma_vector10=Features{1,2}(33,:)';
mean_chroma_vector11=Features{1,2}(34,:)';
mean_chroma_vector12=Features{1,2}(35,:)';


%%%%%%%%%%% median %%%%%%%%%%%%%%%%%%
offset=35;
median_zcr=Features{1,2}(1+offset,:)';
median_energy=Features{1,2}(2+offset,:)';
median_energy_entropy=Features{1,2}(3+offset,:)';
median_spectral_centroid_var=Features{1,2}(4+offset,:)';
median_spectral_centroid_spread=Features{1,2}(5+offset,:)';
median_spectral_entropy=Features{1,2}(6+offset,:)';
median_spectral_flux=Features{1,2}(7+offset,:)';
median_spectral_rolloff=Features{1,2}(8+offset,:)';
median_mfccs1=Features{1,2}(9+offset,:)';
median_mfccs2=Features{1,2}(10+offset,:)';
median_mfccs3=Features{1,2}(11+offset,:)';
median_mfccs4=Features{1,2}(12+offset,:)';
median_mfccs5=Features{1,2}(13+offset,:)';
median_mfccs6=Features{1,2}(14+offset,:)';
median_mfccs7=Features{1,2}(15+offset,:)';
median_mfccs8=Features{1,2}(16+offset,:)';
median_mfccs9=Features{1,2}(17+offset,:)';
median_mfccs10=Features{1,2}(18+offset,:)';
median_mfccs11=Features{1,2}(19+offset,:)';
median_mfccs12=Features{1,2}(20+offset,:)';
median_mfccs13=Features{1,2}(21+offset,:)';
median_harmonic_hr=Features{1,2}(22+offset,:)';
median_harmonic_f0=Features{1,2}(23+offset,:)';
median_chroma_vector1=Features{1,2}(24+offset,:)';
median_chroma_vector2=Features{1,2}(25+offset,:)';
median_chroma_vector3=Features{1,2}(26+offset,:)';
median_chroma_vector4=Features{1,2}(27+offset,:)';
median_chroma_vector5=Features{1,2}(28+offset,:)';
median_chroma_vector6=Features{1,2}(29+offset,:)';
median_chroma_vector7=Features{1,2}(30+offset,:)';
median_chroma_vector8=Features{1,2}(31+offset,:)';
median_chroma_vector9=Features{1,2}(32+offset,:)';
median_chroma_vector10=Features{1,2}(33+offset,:)';
median_chroma_vector11=Features{1,2}(34+offset,:)';
median_chroma_vector12=Features{1,2}(35+offset,:)';



%%%%%%%%%%% std %%%%%%%%%%%%%%%%%%
offset=70;
std_zcr=Features{1,2}(1+offset,:)';
std_energy=Features{1,2}(2+offset,:)';
std_energy_entropy=Features{1,2}(3+offset,:)';
std_spectral_centroid_var=Features{1,2}(4+offset,:)';
std_spectral_centroid_spread=Features{1,2}(5+offset,:)';
std_spectral_entropy=Features{1,2}(6+offset,:)';
std_spectral_flux=Features{1,2}(7+offset,:)';
std_spectral_rolloff=Features{1,2}(8+offset,:)';
std_mfccs1=Features{1,2}(9+offset,:)';
std_mfccs2=Features{1,2}(10+offset,:)';
std_mfccs3=Features{1,2}(11+offset,:)';
std_mfccs4=Features{1,2}(12+offset,:)';
std_mfccs5=Features{1,2}(13+offset,:)';
std_mfccs6=Features{1,2}(14+offset,:)';
std_mfccs7=Features{1,2}(15+offset,:)';
std_mfccs8=Features{1,2}(16+offset,:)';
std_mfccs9=Features{1,2}(17+offset,:)';
std_mfccs10=Features{1,2}(18+offset,:)';
std_mfccs11=Features{1,2}(19+offset,:)';
std_mfccs12=Features{1,2}(20+offset,:)';
std_mfccs13=Features{1,2}(21+offset,:)';
std_harmonic_hr=Features{1,2}(22+offset,:)';
std_harmonic_f0=Features{1,2}(23+offset,:)';
std_chroma_vector1=Features{1,2}(24+offset,:)';
std_chroma_vector2=Features{1,2}(25+offset,:)';
std_chroma_vector3=Features{1,2}(26+offset,:)';
std_chroma_vector4=Features{1,2}(27+offset,:)';
std_chroma_vector5=Features{1,2}(28+offset,:)';
std_chroma_vector6=Features{1,2}(29+offset,:)';
std_chroma_vector7=Features{1,2}(30+offset,:)';
std_chroma_vector8=Features{1,2}(31+offset,:)';
std_chroma_vector9=Features{1,2}(32+offset,:)';
std_chroma_vector10=Features{1,2}(33+offset,:)';
std_chroma_vector11=Features{1,2}(34+offset,:)';
std_chroma_vector12=Features{1,2}(35+offset,:)';


%%%%%%%%%%% stdbymean %%%%%%%%%%%%%%%%%%
offset=70+35;
stdbymean_zcr=Features{1,2}(1+offset,:)';
stdbymean_energy=Features{1,2}(2+offset,:)';
stdbymean_energy_entropy=Features{1,2}(3+offset,:)';
stdbymean_spectral_centroid_var=Features{1,2}(4+offset,:)';
stdbymean_spectral_centroid_spread=Features{1,2}(5+offset,:)';
stdbymean_spectral_entropy=Features{1,2}(6+offset,:)';
stdbymean_spectral_flux=Features{1,2}(7+offset,:)';
stdbymean_spectral_rolloff=Features{1,2}(8+offset,:)';
stdbymean_mfccs1=Features{1,2}(9+offset,:)';
stdbymean_mfccs2=Features{1,2}(10+offset,:)';
stdbymean_mfccs3=Features{1,2}(11+offset,:)';
stdbymean_mfccs4=Features{1,2}(12+offset,:)';
stdbymean_mfccs5=Features{1,2}(13+offset,:)';
stdbymean_mfccs6=Features{1,2}(14+offset,:)';
stdbymean_mfccs7=Features{1,2}(15+offset,:)';
stdbymean_mfccs8=Features{1,2}(16+offset,:)';
stdbymean_mfccs9=Features{1,2}(17+offset,:)';
stdbymean_mfccs10=Features{1,2}(18+offset,:)';
stdbymean_mfccs11=Features{1,2}(19+offset,:)';
stdbymean_mfccs12=Features{1,2}(20+offset,:)';
stdbymean_mfccs13=Features{1,2}(21+offset,:)';
stdbymean_harmonic_hr=Features{1,2}(22+offset,:)';
stdbymean_harmonic_f0=Features{1,2}(23+offset,:)';
stdbymean_chroma_vector1=Features{1,2}(24+offset,:)';
stdbymean_chroma_vector2=Features{1,2}(25+offset,:)';
stdbymean_chroma_vector3=Features{1,2}(26+offset,:)';
stdbymean_chroma_vector4=Features{1,2}(27+offset,:)';
stdbymean_chroma_vector5=Features{1,2}(28+offset,:)';
stdbymean_chroma_vector6=Features{1,2}(29+offset,:)';
stdbymean_chroma_vector7=Features{1,2}(30+offset,:)';
stdbymean_chroma_vector8=Features{1,2}(31+offset,:)';
stdbymean_chroma_vector9=Features{1,2}(32+offset,:)';
stdbymean_chroma_vector10=Features{1,2}(33+offset,:)';
stdbymean_chroma_vector11=Features{1,2}(34+offset,:)';
stdbymean_chroma_vector12=Features{1,2}(35+offset,:)';



%%%%%%%%%%% max %%%%%%%%%%%%%%%%%%
offset=70+70;
max_zcr=Features{1,2}(1+offset,:)';
max_energy=Features{1,2}(2+offset,:)';
max_energy_entropy=Features{1,2}(3+offset,:)';
max_spectral_centroid_var=Features{1,2}(4+offset,:)';
max_spectral_centroid_spread=Features{1,2}(5+offset,:)';
max_spectral_entropy=Features{1,2}(6+offset,:)';
max_spectral_flux=Features{1,2}(7+offset,:)';
max_spectral_rolloff=Features{1,2}(8+offset,:)';
max_mfccs1=Features{1,2}(9+offset,:)';
max_mfccs2=Features{1,2}(10+offset,:)';
max_mfccs3=Features{1,2}(11+offset,:)';
max_mfccs4=Features{1,2}(12+offset,:)';
max_mfccs5=Features{1,2}(13+offset,:)';
max_mfccs6=Features{1,2}(14+offset,:)';
max_mfccs7=Features{1,2}(15+offset,:)';
max_mfccs8=Features{1,2}(16+offset,:)';
max_mfccs9=Features{1,2}(17+offset,:)';
max_mfccs10=Features{1,2}(18+offset,:)';
max_mfccs11=Features{1,2}(19+offset,:)';
max_mfccs12=Features{1,2}(20+offset,:)';
max_mfccs13=Features{1,2}(21+offset,:)';
max_harmonic_hr=Features{1,2}(22+offset,:)';
max_harmonic_f0=Features{1,2}(23+offset,:)';
max_chroma_vector1=Features{1,2}(24+offset,:)';
max_chroma_vector2=Features{1,2}(25+offset,:)';
max_chroma_vector3=Features{1,2}(26+offset,:)';
max_chroma_vector4=Features{1,2}(27+offset,:)';
max_chroma_vector5=Features{1,2}(28+offset,:)';
max_chroma_vector6=Features{1,2}(29+offset,:)';
max_chroma_vector7=Features{1,2}(30+offset,:)';
max_chroma_vector8=Features{1,2}(31+offset,:)';
max_chroma_vector9=Features{1,2}(32+offset,:)';
max_chroma_vector10=Features{1,2}(33+offset,:)';
max_chroma_vector11=Features{1,2}(34+offset,:)';
max_chroma_vector12=Features{1,2}(35+offset,:)';




%%%%%%%%%%% min %%%%%%%%%%%%%%%%%%
offset=70+70+35;
min_zcr=Features{1,2}(1+offset,:)';
min_energy=Features{1,2}(2+offset,:)';
min_energy_entropy=Features{1,2}(3+offset,:)';
min_spectral_centroid_var=Features{1,2}(4+offset,:)';
min_spectral_centroid_spread=Features{1,2}(5+offset,:)';
min_spectral_entropy=Features{1,2}(6+offset,:)';
min_spectral_flux=Features{1,2}(7+offset,:)';
min_spectral_rolloff=Features{1,2}(8+offset,:)';
min_mfccs1=Features{1,2}(9+offset,:)';
min_mfccs2=Features{1,2}(10+offset,:)';
min_mfccs3=Features{1,2}(11+offset,:)';
min_mfccs4=Features{1,2}(12+offset,:)';
min_mfccs5=Features{1,2}(13+offset,:)';
min_mfccs6=Features{1,2}(14+offset,:)';
min_mfccs7=Features{1,2}(15+offset,:)';
min_mfccs8=Features{1,2}(16+offset,:)';
min_mfccs9=Features{1,2}(17+offset,:)';
min_mfccs10=Features{1,2}(18+offset,:)';
min_mfccs11=Features{1,2}(19+offset,:)';
min_mfccs12=Features{1,2}(20+offset,:)';
min_mfccs13=Features{1,2}(21+offset,:)';
min_harmonic_hr=Features{1,2}(22+offset,:)';
min_harmonic_f0=Features{1,2}(23+offset,:)';
min_chroma_vector1=Features{1,2}(24+offset,:)';
min_chroma_vector2=Features{1,2}(25+offset,:)';
min_chroma_vector3=Features{1,2}(26+offset,:)';
min_chroma_vector4=Features{1,2}(27+offset,:)';
min_chroma_vector5=Features{1,2}(28+offset,:)';
min_chroma_vector6=Features{1,2}(29+offset,:)';
min_chroma_vector7=Features{1,2}(30+offset,:)';
min_chroma_vector8=Features{1,2}(31+offset,:)';
min_chroma_vector9=Features{1,2}(32+offset,:)';
min_chroma_vector10=Features{1,2}(33+offset,:)';
min_chroma_vector11=Features{1,2}(34+offset,:)';
min_chroma_vector12=Features{1,2}(35+offset,:)';

GameTable=table(mean_zcr, mean_energy, ...
        mean_energy_entropy, mean_spectral_centroid_var, ...
        mean_spectral_centroid_spread, mean_spectral_entropy, ...
        mean_spectral_flux, mean_spectral_rolloff, ...
        mean_mfccs1, mean_mfccs2, ...
        mean_mfccs3, mean_mfccs4, ...
        mean_mfccs5, mean_mfccs6, ...
        mean_mfccs7, mean_mfccs8, ...
        mean_mfccs9, mean_mfccs10, ...
        mean_mfccs11, mean_mfccs12, ...
        mean_mfccs13, mean_harmonic_hr, ...
        mean_harmonic_f0, mean_chroma_vector1, ...
        mean_chroma_vector2, mean_chroma_vector3, ...
        mean_chroma_vector4, mean_chroma_vector5, ...
        mean_chroma_vector6, mean_chroma_vector7, ...
        mean_chroma_vector8, mean_chroma_vector9, ...
        mean_chroma_vector10, mean_chroma_vector11, ...
        mean_chroma_vector12, ...  
        median_zcr, median_energy, ...
        median_energy_entropy, median_spectral_centroid_var, ...
        median_spectral_centroid_spread, median_spectral_entropy, ...
        median_spectral_flux, median_spectral_rolloff, ...
        median_mfccs1, median_mfccs2, ...
        median_mfccs3, median_mfccs4, ...
        median_mfccs5, median_mfccs6, ...
        median_mfccs7, median_mfccs8, ...
        median_mfccs9, median_mfccs10, ...
        median_mfccs11, median_mfccs12, ...
        median_mfccs13, median_harmonic_hr, ...
        median_harmonic_f0, median_chroma_vector1, ...
        median_chroma_vector2, median_chroma_vector3, ...
        median_chroma_vector4, median_chroma_vector5, ...
        median_chroma_vector6, median_chroma_vector7, ...
        median_chroma_vector8, median_chroma_vector9, ...
        median_chroma_vector10, median_chroma_vector11, ...
        median_chroma_vector12, ...  
        std_zcr, std_energy, ...
        std_energy_entropy, std_spectral_centroid_var, ...
        std_spectral_centroid_spread, std_spectral_entropy, ...
        std_spectral_flux, std_spectral_rolloff, ...
        std_mfccs1, std_mfccs2, ...
        std_mfccs3, std_mfccs4, ...
        std_mfccs5, std_mfccs6, ...
        std_mfccs7, std_mfccs8, ...
        std_mfccs9, std_mfccs10, ...
        std_mfccs11, std_mfccs12, ...
        std_mfccs13, std_harmonic_hr, ...
        std_harmonic_f0, std_chroma_vector1, ...
        std_chroma_vector2, std_chroma_vector3, ...
        std_chroma_vector4, std_chroma_vector5, ...
        std_chroma_vector6, std_chroma_vector7, ...
        std_chroma_vector8, std_chroma_vector9, ...
        std_chroma_vector10, std_chroma_vector11, ...
        std_chroma_vector12, ...
        stdbymean_zcr, stdbymean_energy, ...
        stdbymean_energy_entropy, stdbymean_spectral_centroid_var, ...
        stdbymean_spectral_centroid_spread, stdbymean_spectral_entropy, ...
        stdbymean_spectral_flux, stdbymean_spectral_rolloff, ...
        stdbymean_mfccs1, stdbymean_mfccs2, ...
        stdbymean_mfccs3, stdbymean_mfccs4, ...
        stdbymean_mfccs5, stdbymean_mfccs6, ...
        stdbymean_mfccs7, stdbymean_mfccs8, ...
        stdbymean_mfccs9, stdbymean_mfccs10, ...
        stdbymean_mfccs11, stdbymean_mfccs12, ...
        stdbymean_mfccs13, stdbymean_harmonic_hr, ...
        stdbymean_harmonic_f0, stdbymean_chroma_vector1, ...
        stdbymean_chroma_vector2, stdbymean_chroma_vector3, ...
        stdbymean_chroma_vector4, stdbymean_chroma_vector5, ...
        stdbymean_chroma_vector6, stdbymean_chroma_vector7, ...
        stdbymean_chroma_vector8, stdbymean_chroma_vector9, ...
        stdbymean_chroma_vector10, stdbymean_chroma_vector11, ...
        stdbymean_chroma_vector12, ...
        max_zcr, max_energy, ...
        max_energy_entropy, max_spectral_centroid_var, ...
        max_spectral_centroid_spread, max_spectral_entropy, ...
        max_spectral_flux, max_spectral_rolloff, ...
        max_mfccs1, max_mfccs2, ...
        max_mfccs3, max_mfccs4, ...
        max_mfccs5, max_mfccs6, ...
        max_mfccs7, max_mfccs8, ...
        max_mfccs9, max_mfccs10, ...
        max_mfccs11, max_mfccs12, ...
        max_mfccs13, max_harmonic_hr, ...
        max_harmonic_f0, max_chroma_vector1, ...
        max_chroma_vector2, max_chroma_vector3, ...
        max_chroma_vector4, max_chroma_vector5, ...
        max_chroma_vector6, max_chroma_vector7, ...
        max_chroma_vector8, max_chroma_vector9, ...
        max_chroma_vector10, max_chroma_vector11, ...
        max_chroma_vector12, ...
        min_zcr, min_energy, ...
        min_energy_entropy, min_spectral_centroid_var, ...
        min_spectral_centroid_spread, min_spectral_entropy, ...
        min_spectral_flux, min_spectral_rolloff, ...
        min_mfccs1, min_mfccs2, ...
        min_mfccs3, min_mfccs4, ...
        min_mfccs5, min_mfccs6, ...
        min_mfccs7, min_mfccs8, ...
        min_mfccs9, min_mfccs10, ...
        min_mfccs11, min_mfccs12, ...
        min_mfccs13, min_harmonic_hr, ...
        min_harmonic_f0, min_chroma_vector1, ...
        min_chroma_vector2, min_chroma_vector3, ...
        min_chroma_vector4, min_chroma_vector5, ...
        min_chroma_vector6, min_chroma_vector7, ...
        min_chroma_vector8, min_chroma_vector9, ...
        min_chroma_vector10, min_chroma_vector11, ...
        min_chroma_vector12 ...
        );

GameTable.label=repmat('gameplay',41,1);

CombinedSamples=vertcat(SoldierTable, GameTable);

end

