function stpFile( wavFile, windowLength, step )
%function stpFile( x, fs, windowLength, step )
%STPFILE Summary of this function goes here
%   Detailed explanation goes here

[x, fs] = audioread(wavFile);


% convert window length and step from seconds to samples:
windowLength = round(windowLength * fs);
step = round(step * fs);
curPos = 1;
L = length(x);

% compute total number of frames:
numOfFrames = floor((L-windowLength)/step) + 1;

figure;
for i=1:numOfFrames % for each frame
    frame = x(curPos : curPos + windowLength - 1);  % get current frame
    
    % multiply the frame with the hamming window:
    frameW = frame .* window(@hamming, length(frame));
    
    % plot the current (orig) frame
    subplot(2,1,1);
    plot(frame);
    title('Current frame (original)');
    axis([0 length(frameW) -1 1])
    
    % plot windowed frame:
    subplot(2,1,2);
    plot(frameW);
    title('Current frame (windowed)');
    axis([0 length(frameW) -1 1]);
    drawnow;
    pause(0.1);
    % do something with 'frame' here:
    % ...
    % ...
    curPos = curPos + step;
end

