function [ out1, out2, out3 ] = myReadFiles( )
%MYREADFILES Summary of this function goes here
%   Detailed explanation goes here
   
   filenames = {};
    filenames{1} = '../samples/golden-soldier.aif';
    for i = 2:1:14
        filenames{i} = '../samples/soldier-';
        filenames{i} = strcat(filenames{i},int2str(i-1));
        filenames{i} = strcat(filenames{i},'.aif');
    end

    filenames2 = {};
    for i = 0:1:2
        filenames2{i+1} = '../samples/gameplay-';
        filenames2{i+1} = strcat(filenames2{i+1},int2str(i));
        filenames2{i+1} = strcat(filenames2{i+1},'.aif');
    end


   
   
   % read the filenames into cells
   for i = 1:1:14
        [y_soldiers(i,:,:),fs] = audioread(filenames{i});
   end

    % read the two big gameplay audio files
    for i = 1:1:2
        [y_gameplay(i,:,:),fs] = audioread(filenames2{i});
    end

       
    
   
   out1 = y_soldiers;
   out2 = y_gameplay;
   out3 = fs;
   
   
end

