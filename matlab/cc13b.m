
% octave requires this
%pkg load signal


% path the folder containing the audio segments:
strDir = '../samples/2-class/';

% mid term statistics to be used:
Statistics = {'mean','median','std','stdbymean','max','min'};

% short-term and mid-term processing window and step:
stWin = 0.040;
stStep = 0.040;
mtWin = 2.0;
mtStep = 1.0;

% perform feature extraction and store the extracted features:
kNN_model_add_class('model2.mat', 'soldier', [ strDir, 'Soldier/'], Statistics, stWin, stStep, mtWin, mtStep);

kNN_model_add_class('model2.mat', 'gameplay', [ strDir, 'Gameplay/'], Statistics, stWin, stStep, mtWin, mtStep);

% If you want to peak at the results
[Features, classNames, MEAN, STD, Statistics, stWin, stStep, mtWin, mtStep] = kNN_model_load('model2.mat');

% plot features from each class
figure;
subplot(2,1,1);
plot(Features{1})
subplot(2,1,2);
plot(Features{2})

% The hjiang paper (m$ :-( ) used variations of 
% the following. Lets look at them.
% zcr, energy, flux, lpc, etc.
% plot stbymean zcr  for each file from each class
meanzcr1=Features{1,1}(1,:);
meanzcr2=Features{1,2}(1,:);
meanenergy1=Features{1,1}(2,:);
meanenergy2=Features{1,2}(2,:);
meanflux1=Features{1,1}(7,:);
meanflux2=Features{1,2}(7,:);

uber_feature_plot_thing(meanzcr1,meanzcr2,meanenergy1,meanenergy2,meanflux1,meanflux2);


CombinedSamples = mymake_csv(Features);
writetable(CombinedSamples,'combined_samples.csv','QuoteStrings',true);





plotFeaturesFile('../samples/soldier-0.aif',11);
plotFeaturesFile('../samples/soldier-1.aif',11);
plotFeaturesFile('../samples/soldier-2.aif',11);
plotFeaturesFile('../samples/gameplay-1.aif',11);

% is zcr feature 1?
plotFeaturesFile('../samples/soldier-2.aif',1);
plotFeaturesFile('../samples/gameplay-1.aif',1);



scriptClassificationPerformance('model2.mat')

% how does it perform on our samples?

[label1, P1, classNames1] = fileClassification('../samples/2-class/Soldier/soldier-0.aif',1,'model2.mat');
disp(label1);
disp(P1);
disp(classNames1);

[label2, P2, classNames2] = fileClassification('../samples/2-class/Gameplay/gameplay-0.aif',1,'model2.mat');
disp(label2);
disp(P2);
disp(classNames2);


[label3, P3, classNames3] = fileClassification('../samples/2-class/Soldier/soldier-3.aif',1,'model2.mat');
disp(label3);
disp(P3);
disp(classNames3);

