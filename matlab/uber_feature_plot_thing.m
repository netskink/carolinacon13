function uber_feature_plot_thing( meanzcr1, meanzcr2, meanenergy1, meanenergy2, meanflux1, meanflux2 )


figure;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uber feature plot thing.
subplot(4,3,1);
% look at the meanzcr for each sample
% meanzcr1 is the values for the 14 soldier samples
% meanzcr2 is the values for the 40 gameplay samples
SN=1:length(meanzcr1);
scatter( SN,meanzcr1); % plot x-axis as sample #, y as value;
hold on;
SN=1:length(meanzcr2);
scatter( SN, meanzcr2);
title('mean zcr per file sample.');
xlabel('sample');
ylabel('mean zcr');

subplot(4,3,4);
histogram( meanzcr1 )
title('mean zcr for soldier sample file');
xlabel('mean zcr bin');
ylabel('soldier mean zcr bin count');

subplot(4,3,7);
histogram( meanzcr2 )
title('mean zcr for gameplay sample file');
xlabel('mean zcr bin');
ylabel('gameplay mean zcr bin count');


subplot(4,3,10);
% look at the meanzcr only
% meanzcr1 is the values for the 14 soldier samples
% meanzcr2 is the values for the 40 gameplay samples
scatter( meanzcr1,meanzcr1); % plot x-axis as sample #, y as value;
hold on;
scatter( meanzcr2, meanzcr2);
title('mean zcr vs mean zcr.');
xlabel('mean zcr');
ylabel('mean zcr');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uber feature plot thing.
subplot(4,3,2);
% look at the meanenergy for each sample
% meanenergy1 is the values for the 14 soldier samples
% meanenergy2 is the values for the 40 gameplay samples
SN=1:length(meanenergy1);
scatter( SN,meanenergy1); % plot x-axis as sample #, y as value;
hold on;
SN=1:length(meanenergy2);
scatter( SN, meanenergy2);
title('mean energy per file sample.');
xlabel('sample');
ylabel('mean energy');

subplot(4,3,5);
histogram( meanenergy1 )
title('mean energy for soldier sample file');
xlabel('mean energy bin');
ylabel('soldier mean energy bin count');

subplot(4,3,8);
histogram( meanenergy2 )
title('mean energy for gameplay sample file');
xlabel('mean energy bin');
ylabel('gameplay mean energy bin count');


subplot(4,3,11);
% look at the meanenrgy only
% meanenergy1 is the values for the 14 soldier samples
% meanenergy2 is the values for the 40 gameplay samples
scatter( meanenergy1,meanenergy1); % plot x-axis as sample #, y as value;
hold on;
scatter( meanenergy2, meanenergy2);
title('mean energy vs mean energy.');
xlabel('mean energy');
ylabel('mean energy');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uber feature plot thing.
subplot(4,3,3);
% look at the meanflux for each sample
% meanflux1 is the values for the 14 soldier samples
% meanflux2 is the values for the 40 gameplay samples
SN=1:length(meanflux1);
scatter( SN,meanflux1); % plot x-axis as sample #, y as value;
hold on;
SN=1:length(meanflux2);
scatter( SN, meanflux2);
title('mean flux per file sample.');
xlabel('sample');
ylabel('mean flux');

subplot(4,3,6);
histogram( meanflux1 )
title('mean flux for soldier sample file');
xlabel('mean flux bin');
ylabel('soldier mean flux bin count');

subplot(4,3,9);
histogram( meanflux2 )
title('mean flux for gameplay sample file');
xlabel('mean flux bin');
ylabel('gameplay mean flux bin count');


subplot(4,3,12);
% look at the mean flux only
% meanflux1 is the values for the 14 soldier samples
% meanflux2 is the values for the 40 gameplay samples
scatter( meanflux1,meanflux1); % plot x-axis as sample #, y as value;
hold on;
scatter( meanflux2, meanflux2);
title('mean flux vs mean flux.');
xlabel('mean flux');
ylabel('mean flux');
hold off;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure;
scatter3(meanzcr1,meanenergy1,meanflux1);
hold on;
scatter3(meanzcr2,meanenergy2,meanflux2);
title('mean(zcr, energy, flux)');
xlabel('mean zcr');
ylabel('mean energy');
zlabel('mean flux');
hold off;
