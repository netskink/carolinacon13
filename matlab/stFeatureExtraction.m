
% short-term processing technique, where the audio signal is
% broken into possibly overlapping short-term windows (frames)
% and the analaysis is carried out on a frame by frame basis

% let x(n), n=0, 1, ... N-1  be an audio signal, N samples long.
%
% During short term processing we focus each time on a small part 
% (frame) of the signal.  We are following a windowing procedure
% at each step, we multiply  the audio signal with a shifted
% version of a finite duration window function, w(n) ie. a 
% discrete-time signal which is zero oursite a finite duration
% interval.  
%
% The resulting singal xi(n) at the ith processing step is given by:
%
% xi(n) = x(n)w(n-mi), i=0...K-1, 
%    K is the number of rames
%    mi is the shift lag, ie. the number of samples by which the
%    the window is shifted to yield the ith frame.
%
% This implies that xi(n) is zero everywhere except in the region
% of samples whith indices mi, ... mi + WL - 1
%    WL is the length of the moving window in samples.
%
% The value of mi depends upon the hop size (step) Ws of the window.
%
% For example, if the window is shifted by 10ms at each step and the
% sampling frequency Fs is 16kHz then
% mi=i* Ws * Fs = i * 0.01 * 16000 = i * 160 samples
%     i = 0,...,K-1
%
% Furthermore, if WL = 300 samples then
%   the 5th frame (i=4) starts at sample index 160 * 4 = 640
%   ends at sample index 160*4+300-1 = 939

%  The above


function Features = stFeatureExtraction( signal, fs, win, step )
%FEATURES Summary of this function goes here
%   win is short-term window size in seconds
%   step is short-term step in seconds

    % if  stereo ..
    if (size(signal,2)>1)
        signal = (sum(signal,2)/2); % convert to MONO by avg'ing the two signal channels.
    end

    % convert window length and step from seconds to samples:
    windowLength = round(win * fs);
    step = round(step * fs);
    
    curPos = 1;
    L = length(signal);
    
    % compute total number of frames:
    numOfFrames = floor((L-windowLength)/step) + 1;
    % number of features to be computed:
    numOfFeatures = 35;
    Features = zeros(numOfFeatures, numOfFrames);
    Ham = window(@hamming, windowLength);
    mfccParams = feature_mfccs_init(windowLength, fs);
    
    for i=1:numOfFrames % for each frame
        % get current Frame:
        frame = signal(curPos:curPos + windowLength-1);
        frame = frame .* Ham;
        frameFFT = getDFT(frame, fs);
        
        if (sum(abs(frame)) > eps)
        
            % compute time domain features:
            Features(1,i) = feature_zcr(frame);
            Features(2,i) = feature_energy(frame);
            Features(3,i) = feature_energy_entropy(frame, 10);

            % compute freq-domain features:
            if (1 == i) 
                frameFFTPrev = frameFFT;
            end
        
            [Features(4,i) Features(5,i)] = feature_spectral_centroid(frameFFT, fs);
            Features(6, i) = feature_spectral_entropy(frameFFT, 10);
            Features(7, i) = feature_spectral_flux(frameFFT, frameFFTPrev);
            Features(8, i) = feature_spectral_rolloff(frameFFT, 0.90);
            MFCCs = feature_mfccs(frameFFT, mfccParams);
            Features(9:21, i) = MFCCs;

            [HR, F0] = feature_harmonic(frame, fs);
            Features(22, i) = HR;
            Features(23, i) = F0;
            Features(23+1:23+12, i) = feature_chroma_vector(frame, fs);

        
        else
    
            Features(:,i) = zeros(numOfFeatures, 1);
        end
    
        curPos = curPos + step;
        frameFFTPrev = frameFFT;
    
    end
    
    % takes whatever is at 35 and applies a 3rd order filter
    % val[k] = median of x(k-3:k+3)
    Features(35, :) = medfilt1(Features(35, :), 3);
    
    
end

% i dont really like this bit about modifying code outside the function. It seems odd to me.
%Features(35, :) = medfilt1(Features(35, :), 3);

