% complete this later

% The fft function computes the FFT of a specified signal.

% In general, we will want to view either the magnitude or phase values of the
% FFT coefficients, which in Matlab can be determined using the abs and angle
% functions.  

% A variety of windows can be applied to a signal before the
% computation of the FFT using the functions hann, hamming, blackman. For a
% complete list, see the window function help. Time-domain windows can help
% minimize spectral artifacts related to signal truncation.

% The spectrogram function computes a time-frequency plot of a signal where color represents spectral magnitude amplitude.
