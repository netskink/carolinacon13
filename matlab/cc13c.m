disp("start");
%n=0;
%f=0;
%k=0
s=zeros(8,8);
for k=0:7
    for n=0:7
        % rows designate k, columns are time samples
        f=k;
        s(k+1,n+1) = cos(2*pi*f*n/8)-1j*sin(2*pi*f*n/8);
    end
end
disp(s);

% phase = arctan(imaginary component / real component)
thephase=zeros(8,8)
for n=0:7
    thephase(n+1,:) = atan( imag(s(n+1,:)) ./ real(s(n+1,:)) )
end
disp('end')

% plot the phase
figure;
for n=0:7
%    foo=sprintf("k=%d %d",n,mod(n,2));
%    disp(foo);
    subplot(4,2,n+1);
    plot(thephase(n+1,:))
    the_title = sprintf("K=%d",n);
    title(the_title);
end

