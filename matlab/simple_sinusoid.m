% to create a simple sinusoidal signal
fs = 44100;                    % sampling rate
T = 1/fs;                      % sampling period
t = [0:T:0.25];                % time vector

f1 = 50;                       % frequency in Hertz
omega1 = 2*pi*f1;              % angular frequency in radians

phi = 2*pi*0.75;               % arbitrary phase offset = 3/4 cycle
x1 = cos(omega1*t + phi);      % sinusoidal signal, amplitude = 1

plot(t, x1);                   % plot the signal
xlabel('Time (seconds)');
ylabel('x1');
title('Simple Sinusoid');

sound(0.9*x1, fs);             % play the signal

