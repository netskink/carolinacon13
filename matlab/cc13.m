% cc13 talk
disp('cc13 talk');

[y_soldiers, y_gameplay, fs] = myReadFiles();


clip_length = int64(98183/1);

% for some reason, this one gets read as zero sometimes
foo1 = y_soldiers(11,(2*clip_length:(3*clip_length)-1));
%sound(foo2,fs);


soldier_samples = zeros(14,clip_length);  % this includes the golden sample
for i = 1:1:14
    % its recorded in stereo, pull in one channel
    soldier_samples(i,:) = y_soldiers(i,(1:clip_length));
    if i == 11
        % this specific track must have been offset when I exported it. whoops.
        %soldier_samples(11,:) = y_soldiers(11,(2*98183:(3*98183)-1));
        soldier_samples(11,:) = foo1;
    end
end
soldier_golden = soldier_samples(1,:);


% play each sample to ensure its working
% for i = 1:1:14
%     prompt = 'next sound is :';
%     prompt = strcat(prompt, num2str(i));
%     input(prompt);
%     
%     sound(soldier_samples(i,:),fs);
% end
    

% similar thing for the gameplay samples
% here we take these long recordings and break down
% to equivalent length soldier samples
gameplay_samples = zeros(14,clip_length);  % this includes the golden sample
for i = 1:1:2

    % these are the windowed game play samples
    win_len = 98183;
    for j = 1:1:20
        val_start = 1 + (j-1)*win_len;
        val_end = j*win_len;
        samples=[ val_start , val_end ];
        gameplay_samples(20*(i-1)+j,:) = y_gameplay(i,val_start:val_end);
    end    
    % its recorded in stereo, pull in one channel
%    gameplay_samples(i,:) = y_gameplay(i,(1:clip_length));
end




% play each sample to ensure its working
% for i = 1:1:40
%     prompt = 'next sound is :';
%     prompt = strcat(prompt, num2str(i));
%     input(prompt);
% 
%     sound(gameplay_samples(i,:),fs);
% end


clear y_soldiers y_gameplay foo1;
clear gameplay_samples;
clear soldier_samples;


%windowLength = 2;  % time in seconds
%step = 0.001; % step in seconds
%fs2 = 16000;
%stpFile(soldier_samples(1,:),fs2, windowLength, step);
%stpFile(soldier_golden,fs2, windowLength, step);


% 3/4 overlap
%windowLength = 0.02;
%step = 0.005;

% half overlap
%windowLength = 0.02;
%step = 0.01;

% 1/4 overlap 
%windowLength = 0.02;
%step = 0.015;

% no overlap
%windowLength = 0.02;
%step = 0.02;


% 1/3? overlap
windowLength = 0.02;
step = 0.005;


% plot
%stpFile('../samples/golden-soldier.aif', windowLength, step);
myStpFile(soldier_golden, fs, windowLength, step);


disp('done')