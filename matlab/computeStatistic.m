function S = computeStatistic( seq, statistic )
%COMPUTESTATISTIC 
%   Detailed explanation goes here

    if strcmp(statistic, 'mean')
        S = mean(seq);
        return;
    end
    
    if strcmp(statistic, 'median')
        S = median(seq);
        return;
    end

    if strcmp(statistic, 'std')
        S = std(seq);
        return;
    end

    if strcmp(statistic, 'stdbymean')
        % eps = 2^-52
        % used for floating point relative accuracy.
        % maybe he is using it as a small number so never divide by zero?
        S = std(seq) ./ (mean(seq)+eps);
        return;
    end

    if strcmp(statistic, 'max')
        S = max(seq);
        return;
    end
    if strcmp(statistic, 'min')
        S = min(seq);
        return;
    end
    if strcmp(statistic, 'meanNonZero')
        for i=1:size(seq,2)
            curSeq = seq(:,i)
            S(i) = mean(curSeq(curSeq>0));
        end
        return;
    end

    if strcmp(statistic, 'medianNonZero')
        for i=1:size(seq,2)
            curSeq = seq(:,i)
            S(i) = median(curSeq(curSeq>0));
        end
        return;
    end
    
end

