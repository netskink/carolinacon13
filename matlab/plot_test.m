% plot test

X=[2, 4, 6, 8, 10];
Y=[1, 3, 5, 7, 9];
T=[1, 2, 3, 4, 5];

figure;

subplot(2,1,1);
acolor = [255,0,0];  % plot the first points as red
% colors should be a Nx3 matrix
colors = repmat(acolor,5,1);
scatter(X,T,[],colors);
hold on;
acolor = [0,255,0];  % plot the second set of points as green
% colors should be a Nx3 matrix
colors = repmat(acolor,5,1);
scatter(Y,T,[],colors);

subplot(2,1,2);
plot(X);
hold on;
plot(Y);
