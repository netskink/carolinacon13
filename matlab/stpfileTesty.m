
% test for stpfile program
disp('stpfile test');

% octave requires this
%pkg load control
%pkg load signal



% this will be the full sample in stereo
[golden0,fs] = audioread('../samples/golden-soldier.aif');

clip_length = int64(98183/1);

% rather than converting to mono by taking average, just take one channel
% and discard the other.
golden = zeros(1,clip_length);
golden = golden0(1:clip_length,1);

% 3/4 overlap
%windowLength = 0.02;
%step = 0.005;

% half overlap
%windowLength = 0.02;
%step = 0.01;

% 1/4 overlap 
%windowLength = 0.02;
%step = 0.015;

% no overlap
%windowLength = 0.02;
%step = 0.02;


% 1/3? overlap
windowLength = 0.02;
step = 0.005;


% plot
%stpFile('../samples/golden-soldier.aif', windowLength, step);
myStpFile(golden, fs, windowLength, step);


disp('done')