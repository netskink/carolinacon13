
%
% x,y is the raw value returned by the controller
% xd,yd is adjusted value
%


% The setup
% x1d = x1 * A  + y1 * B  + C
% x2d = x2 * A  + y2 * B  + C
% x3d = x3 * A  + y3 * B  + C
%
% y1d = x1 * D  + y1 * E  + F
% y2d = x2 * D  + y2 * E  + F
% y3d = x3 * D  + y3 * E  + F
%
%
% [x1d; x2d; x3d] = Z*[A; B; C]
% [y1d; y2d; y3d] = Z*[D; E; F]
%
%
% Z = [ x1  y1  1;
%       x2  y2  1;
%       x3  y3  1;];
%
x1 = 650;
x2 = 2800;
x3 = 2640;
y1 = 2000;
y2 = 1350;
y3 = 3500;
X = [x1; x2; x3 ]; 
Y = [y1; y2; y3 ]; 
Z = [X,Y, [1;1;1;]];

% This results in:
%Z = [ 650  2000  1;
%     2800  1350  1;
%     2640  3500  1 ];
%
% Xd = Z * [A; B; C];
% Yd = Z * [D; E; F];
%
% Solving for ABC and DEF
% [A;B;C] = invZ *Xd
% [D;E;F] = invZ *Yd

     
% linsolve returns the solution
% to A * x = b
% [x, R] = linsolve(A,b);
% R is zero when singular


% Rewriting to form of eq for linsolve
%
% A*x=b where A and b are known
% invZ*X = [A;B;C]
% A = invZ
% x = X
[Xd,r] = linsolve(inv(Z),X)
[Yd,r] = linsolve(inv(Z),Y)

% That does not work.
% how about consider all results
%  [A, D;          [x1d, y1d
%   B, E; = invZ *  x2d, y2d
%   C, F]           x3d, y3d ]

[D, r] = linsolve(inv(Z), [X,Y])  
%  [A;          [x1d;
%   B; = invZ *  x2d;
%   C;           x3d;
%   D;           y1d;
%   E;           y2d;
%   F; ]         y3d ]

[D2, r] = linsolve(inv(Z), [X;Y])  



