
% demo the generation of synthetic tones


Fs = 16000;  % sampling frequency, sample periodogram
Ts = 1/Fs;  
time = 0:Ts:01.;            % define when sampling occurs
                            % (sampling lasts for 0.1 secs):
                   
Freqs = [250 550 900];      % these are the frequencies of the signals
Xs = zeros(length(Freqs), length(time));

for i=1: length(Freqs)      % create one audio signal (tone) per frequency
    Xs(i,:) = cos(2*pi*Freqs(i)*time);
end

x = sum(Xs);                % The final sum of the tones
x = x ./ max(abs(x));       % Normalize x

figure; 
plot(time,x); 
axis([0 time(end) -1 1 ]);  %plot X:
xlabel('Time (sec)'); 
ylabel('Signal Ampl.');
title('A simple audio signal');


sound(x,Fs)

