function [ out1,out2,out3 ] = myAssignSignals( y_soldiers, y_gameplay, fs )
%MYASSIGNSIGNALS I dont use this anymore
%   Detailed explanation goes here

    %win_len = 5*fs
    % win_len = 392730
    % for i = 1:1:2
    %     val_start = 1 + (i-1)*win_len;
    %     val_end = i*win_len;
    %     samples=[ val_start , val_end ];
    %     y_samples_gameplay(i,:) = y_gameplay(1,val_start:val_end);
    % end

    % int64(196365/2) = 98183
    %sound(y_soldiers(1,(1:98183)),fs)  % default sample rate is 8Khz
    %sound(y_soldiers(2,(1:98183)),fs)  % default sample rate is 8Khz

    % these are the soldiers sounds. 
    out1 = {};
    for i = 1:1:14
        out1{i} = y_soldiers(i,(1:98183));
    end

    % these are the long game play songs
    out2 = {};
    for i = 1:1:2
        out2{i} = y_gameplay(i,(1:98183));
    end

    
    % these are the windowed game play samples
    out3 = {};
    %win_len = 392730;
    win_len = 98183;
    for i = 1:1:20
        val_start = 1 + (i-1)*win_len;
        val_end = i*win_len;
        samples=[ val_start , val_end ];
        y_samples_gameplay(i,:) = y_gameplay(1,val_start:val_end);
        out3{i} = y_samples_gameplay(i,:);
    end


end

