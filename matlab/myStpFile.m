function myStpFile( x, fs, windowLength, step )


% convert window length and step from seconds to samples:
windowLength = round(windowLength * fs);
step = round(step * fs);
curPos = 1;
L = length(x);

% compute total number of frames:
numOfFrames = floor((L-windowLength)/step) + 1;

figure;
for i=1:numOfFrames % for each frame
    frame = x(curPos : curPos + windowLength - 1);  % get current frame
    
    % multiply the frame with the hamming window:
    frameW1 = frame .* window(@rectwin, length(frame));
    frameW2 = frame .* window(@hamming, length(frame));
    frameW3 = frame .* window(@gausswin, length(frame));
    
    % plot the current (orig) frame
    subplot(4,1,1);
    plot(frame);
    title('Current frame (original)');
    axis([0 length(frameW1) -1 1])
    
    % plot windowed frame:
    subplot(4,1,2);
    plot(frameW1);
    title('Current frame (windowed - rectangle)');
    axis([0 length(frameW1) -1 1]);
    drawnow;
    pause(0.1);
    
    % plot windowed frame:
    subplot(4,1,3);
    plot(frameW2);
    title('Current frame (windowed - hamming)');
    axis([0 length(frameW1) -1 1]);
    drawnow;
    pause(0.1);
    
    % plot windowed frame:
    subplot(4,1,4);
    plot(frameW3);
    title('Current frame (windowed - gauss)');
    axis([0 length(frameW1) -1 1]);
    drawnow;
    pause(0.1);
    
    
    % do something with 'frame' here:
    % ...
    % ...
    %wvtool(frame, sum(frameW,2));  % a column vector of the sums of each row
%    foo = sum(frameW,1)'
%    fooN = ((foo-mean(foo))/std(foo));
    
%    frameFoo = (frame-mean(frame))/std(frame);
%    wvtool(frameFoo, fooN);  % a column vector of the sums of each row



    curPos = curPos + step;
end

