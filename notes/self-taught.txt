This is from Quora

https://www.quora.com/Are-you-a-self-taught-machine-learning-engineer-If-yes-how-did-you-do-it-and-how-long-did-it-take-you

Guy #1

Nikos Kostagiolas, Undergraduate Research Assistant at NCSR Demokritos

Updated Aug 30, 2016 · Upvoted by Rahul Bohare, M.S. Machine Learning &
Robotics, Technical University of Munich (2018) Well, it all started by
noticing that Andrew Ng had a MOOC in Coursera called “Machine Learning”. This
was the first time I came in contact with Machine Learning theory and its
applications and, as it was rather complete for an introduction, it helped me
make my first steps in understanding the most striking key concepts in the
field.

Then, I realized that I had to strengthen my skills in math (espacially Linear
Algebra, Calculus and Probability Theory), in order to delve deeper into ML
theory, so I considered reading books such as Pattern Recognition and Machine
Learning by Christopher Bishop and Machine Learning by Tom Mitchel along with
watching some MOOCs in math. For example, Gilbert Strang’s lectures on Linear
Algebra never get old and can be accessed on YouTube, Single Variable Calculus
and Multi-Variate Calculus also on YouTube and Harvard’s Statistics 110.

Then I started to familiarize myself with some key languages which I thought
would suffice for my future ML application developing such as Python, Java, R
and Matlab. Although I had reviewed them during my BSc studies, I thought that
revising them was a nice opportunity to refresh my skills and maybe check other
problems that I had skipped previously. I would especially recommend
implementing a solution to a classification problem in Python using the
scikit-learn library at this point (maybe something in text mining), in order
to get a first touch with ML engineering.

Finally, I became interested in Neural Networks and Deep Learning. However, due
to the fact that modules of this kind weren’t a part of our BSc curriculum I
figured out that the best solution was to check Geoffrey Hinton’s MOOC on
Coursera conserning Neural Networks in order to get a nice grasp from maybe one
of the top experts in the field. Later on, I went through reading and
implementing the code in Nielsen’s Deep Learning and Neural Networks online
book, which was life-changing for me as it is so nicely written, compact and
easy to understand. In addition to that, I delved into Deep Learning library
tutorials (e.g. Theano, Lasagne and Caffe), in order to give myself some
flexibility concerning which library I could use depending on the problem. I
would also like to add that a nice overview of Deep Learning is given in this
book of the same name by Goodfellow, Bengio and Courville, which also covers
the math fundamentals needed to understand how Deep Learning works.

The above procedure gave me the chance to be an undergraduate Research
Assistant at NCSR Demokritos, the most prestigious research institution in
Greece.



Guy #2

It’s November 2012. With a much shorter resume and much longer hair I stand in
a lobby of some business center in Saint-Petersburg, I can’t even remember its
address, but I remember that day.

He goes on for 16 paragraphs about his carear - e.g about working on Kaggle
competitions and then says:

Now I understand that after those four years I still know nothing. But that’s
okay. It will always be that way, because for some people it’s never enough.
Four years are nothing. Maybe ten will feel more substantial, but I doubt it.
I’ve just started, in a very carefree way, playing with numbers and having fun.
When you stop worrying about things that don’t matter that much, about how
robots will take our jobs and WoW accounts and make our lives miserable, you
can do something interesting. Sure, there will be people who will like it and
find it useful.



Guy #3


When I first started here are the skills that I didn’t have

  o  Coding
  o  Statistics
  o  Understanding of ML algorithms

Basically all of Data Science skills

Of the tools that I used today I had:

  o  Desire to learn
  o  Vague idea of Regression
  o  A computer

Basically a desire to learn and remembering y = mx+b from high school.

Here are the tools I used to increase my toolbox (in order of use):

  o  Code Academy
  o  DataCamp
  o  Learning Python The Hard Way
  o  Coursera Machine Learning Package
  o  Udacity Machine Learning Nano Degree
  *  Udemy
     *  Complete Python Bootcamp
     *  Data Analysis
     *  Data Science
  *  Machine Learning Mastery
  *  General Assembly Part Time Course
  *  Metis Data Science BootCamp
  *  Cold Emailing

That looks like a lot!!! But to be honest only the bolded ones were actually
useful and the others only taught me the faults of online education. I spent
6-7 months on the first 5 tools and that’s where I think others can make this
more efficient.


Guy #4

a guide to machine learning online classes.  He also has a list of courses for other subjects,
namely big data and stats.

https://medium.freecodecamp.com/every-single-machine-learning-course-on-the-internet-ranked-by-your-reviews-3c4a7b8026c0

